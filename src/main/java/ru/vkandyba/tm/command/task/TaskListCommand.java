package ru.vkandyba.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.ITaskService;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.enumerated.Sort;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list...";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS]");
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final String sort = TerminalUtil.nextLine();

        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll(userId);
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(userId, sortType.getComparator());
        }
        for (Task task : tasks) {
            System.out.println(tasks.indexOf(task) + 1 + ". " + task.getName() + " " + task.getId() + ": " + task.getDescription());
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
