package ru.vkandyba.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.ICommandRepository;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.constant.ArgumentConst;
import ru.vkandyba.tm.constant.TerminalConst;
import ru.vkandyba.tm.model.Command;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@NotNull String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArg(@NotNull String arg) {
        return arguments.get(arg);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getListCommandName() {
        @NotNull final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            @Nullable final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getListCommandArg() {
        @NotNull final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            @Nullable final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }
}
