package ru.vkandyba.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @NotNull
    @Override
    public Boolean existsByIndex(@NotNull Integer index) {
        if (index < 0) return false;
        return index < list.size();
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        return entity != null;
    }

    @Nullable
    @Override
    public E findById(@NotNull String id) {
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull Integer index) {
        return list.get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @NotNull
    @Override
    public E add(@NotNull E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(@NotNull E entity) {
        list.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return list;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull Comparator<E> comparator) {
        return list.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

}
