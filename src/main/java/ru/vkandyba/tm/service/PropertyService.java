package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final String FILE_NAME = "config.properties";

    @NotNull
    private final String APPLICATION_VERSION = "application.version";

    @NotNull
    private final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private final String DEVELOPER_NAME = "developer.name";

    @NotNull
    private final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private final String DEVELOPER_EMAIL = "developer.email";

    @NotNull
    private final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    private final String HASH_SECRET = "hash.secret";

    @NotNull
    private final String HASH_SECRET_DEFAULT = "";

    @NotNull
    private final String HASH_ITERATION = "hash.iteration";

    @NotNull
    private final String HASH_ITERATION_DEFAULT = "1";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService(){
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if(inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getValue(@NotNull final String name, @NotNull final String defaultName){
        @Nullable final String systemProperty = System.getProperty(name);
        if(systemProperty != null) return systemProperty;
        @Nullable final String envProperty = System.getenv(name);
        if(envProperty != null) return envProperty;
        return properties.getProperty(name, defaultName);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return getValue(DEVELOPER_NAME, DEVELOPER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return getValue(DEVELOPER_EMAIL, DEVELOPER_EMAIL_DEFAULT);
    }

    @NotNull
    @Override
    public String getVersion() {
        return getValue(APPLICATION_VERSION, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getHashSecret() {
        return getValue(HASH_SECRET, HASH_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getHashIteration() {
        return getValue(HASH_ITERATION, HASH_ITERATION_DEFAULT);
    }

}
